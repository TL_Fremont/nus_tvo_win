// This file is for optical flow class
//
// Bi Yingcai, 2017-11-02

#include "lkflow.h"

namespace  lkflow {

    LKFlow::LKFlow()
    {
        need_init_ = true;
        
        for ( int i = 0; i < 2 * params_.total_subregions;
            i++ )
        { subregion_feature_num_.push_back ( 0 ); }
    }
    
    LKFlow::~LKFlow()
    {
    }
    
    void LKFlow::featureTracking ( Mat &img_prev,
        Mat &img_cur, vector<Point2f> &points_prev,
        vector<Point2f> &points_cur )
    {
        // set klt flow parameters
        TermCriteria termcrit ( CV_TERMCRIT_ITER |
            CV_TERMCRIT_EPS, 20, 0.03 );
        Size subPixWinSize ( 10, 10 ), winSize ( 31, 31 );
        
        // copy last points_cur to points_prev
        std::swap ( points_cur, points_prev );
        
        // first image, need to init first
        if ( need_init_ ) {
            need_init_ = false;
            img_cur.copyTo ( img_prev );
            return;
        }
        else if ( !img_prev.empty() ) {
            vector<uchar> status;
            vector<float> err;
            
            int py_subregions = params_.y_subregions;
            int px_subregions = params_.x_subregions;
            int px_boundary = params_.x_boundary;
            int py_boundary = params_.y_boundary;
            int psubregion_width = params_.subregion_width;
            int psubregion_height = params_.subregion_height;
            int pmax_subregion_features =
                params_.max_subregion_features;
            int pimage_width = params_.image_width;
            int pimage_height = params_.image_height;
            
            // traverse subregions
            for ( int i = 0; i < py_subregions; i++ ) {
                for ( int j = 0; j < px_subregions; j++ ) {
                    // find subregion index
                    int ind = px_subregions * i + j;
                    
                    // find feature number to be detected
                    int num_to_find = pmax_subregion_features -
                        subregion_feature_num_[ind];
                        
                    if ( num_to_find > 0 ) {
                        // retrieve region of interest
                        int subregion_left = px_boundary +
                            psubregion_width * j;
                        int subregion_top = py_boundary +
                            psubregion_height * i;
                        Rect roi ( subregion_left, subregion_top,
                            psubregion_width, psubregion_height );
                        Mat img_roi = img_prev ( roi );
                        vector<Point2f> points_roi;
                        
                        // detect good features to track inside roi
                        goodFeaturesToTrack ( img_roi, points_roi,
                            num_to_find, 0.01, 10, Mat(), 3, 0, 0.04 );
                            
                        if ( !points_roi.empty() )
                        { cornerSubPix ( img_roi, points_roi, subPixWinSize, Size ( -1, -1 ), termcrit ); }
                        
                        // calculate new feature number in region
                        subregion_feature_num_[ind] += points_roi.size();
                        
                        // remap feature to image coordinate
                        for ( int i = 0; i < points_roi.size(); i++ ) {
                            points_roi[i].x += subregion_left;
                            points_roi[i].y += subregion_top;
                            points_prev.push_back ( points_roi[i] );
                        }
                    }
                }
            }
            
            // calculate klt optical flow
            calcOpticalFlowPyrLK ( img_prev, img_cur,
                points_prev, points_cur, status, err, winSize, 3,
                termcrit, 0, 0.001 );
                
            // getting rid of points for which the KLT tracking failed or those who have gone outside the frame
            int indexCorrection = 0;
            
            for ( size_t i = 0; i < status.size(); i++ ) {
                Point2f pt = points_cur.at ( i -
                        indexCorrection );
                        
                if ( ( status.at ( i ) == 0 ) ||
                    ( pt.x < px_boundary ) ||
                    ( pt.y < px_boundary ) ||
                    ( pt.x > pimage_width - px_boundary ) ||
                    ( pt.y > pimage_height - py_boundary ) ) {
                    status.at ( i ) = 0;
                    points_prev.erase ( points_prev.begin() +
                        ( i - indexCorrection ) );
                    points_cur.erase ( points_cur.begin() +
                        ( i - indexCorrection ) );
                    indexCorrection++;
                }
            }
            
            // clean subregion feature number
            for ( int i = 0;
                i < px_subregions * py_subregions; i++ ) {
                subregion_feature_num_[i] = 0;
            }
            
            // recount subregion feature number
            for ( size_t i = 0; i < points_cur.size(); i++ ) {
                int x_ind = ( points_cur[i].x - px_boundary ) /
                    psubregion_width;
                int y_ind = ( points_cur[i].y - py_boundary ) /
                    psubregion_height;
                int ind = y_ind * px_subregions + x_ind;
                subregion_feature_num_[ind] ++;
            }
        }
        
        // copy current image to previous image
        cv::swap ( img_cur, img_prev );
    }
    
    Mat LKFlow::B2Gmat ( float* att )
    {
        Mat result = Mat::zeros ( 3, 3,
                DataType<double>::type );
                
        float sina = ::sin ( att[0] );
        double sinb = ::sin ( att[1] );
        double sinc = ::sin ( att[2] );
        float cosa = ::cos ( att[0] );
        double cosb = ::cos ( att[1] );
        double cosc = ::cos ( att[2] );
        
        result.at<double> ( 0, 0 ) = cosb * cosc;
        result.at<double> ( 1, 0 ) = cosb * sinc;
        result.at<double> ( 2, 0 ) = -sinb;
        
        result.at<double> ( 0,
            1 ) = sina * sinb * cosc - cosa * sinc;
        result.at<double> ( 1,
            1 ) = sina * sinb * sinc + cosa * cosc;
        result.at<double> ( 2, 1 ) = sina * cosb;
        
        result.at<double> ( 0,
            2 ) = cosa * sinb * cosc + sina * sinc;
        result.at<double> ( 1,
            2 ) = cosa * sinb * sinc - sina * cosc;
        result.at<double> ( 2, 2 ) = cosa * cosb;
        
        return result;
    }
    
    Mat LKFlow::G2Bmat ( float* att )
    {
        Mat result = Mat::zeros ( 3, 3,
                DataType<double>::type );
                
        float sina = ::sin ( att[0] );
        float sinb = ::sin ( att[1] );
        float sinc = ::sin ( att[2] );
        float cosa = ::cos ( att[0] );
        float cosb = ::cos ( att[1] );
        float cosc = ::cos ( att[2] );
        
        result.at<double> ( 0, 0 ) = cosb * cosc;
        result.at<double> ( 0, 1 ) = cosb * sinc;
        result.at<double> ( 0, 2 ) = -sinb;
        
        result.at<double> ( 1,
            0 ) = sina * sinb * cosc - cosa * sinc;
        result.at<double> ( 1,
            1 ) = sina * sinb * sinc + cosa * cosc;
        result.at<double> ( 1, 2 ) = sina * cosb;
        
        result.at<double> ( 2,
            0 ) = cosa * sinb * cosc + sina * sinc;
        result.at<double> ( 2,
            1 ) = cosa * sinb * sinc - sina * cosc;
        result.at<double> ( 2, 2 ) = cosa * cosb;
        
        return result;
    }
    
    void LKFlow::fcnDispDoubleMat ( Mat& A )
    {
        printf ( "\n" );
        
        for ( int i = 0; i < A.rows; i++ ) {
            for ( int j = 0; j < A.cols; j++ ) {
                printf ( "%f  ", A.at<double> ( i, j ) );
            }
            
            printf ( "\n" );
        }
    }
    
}

