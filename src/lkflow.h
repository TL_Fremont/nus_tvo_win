//  This file is for optical flow class
//
// Bi Yingcai, 2017-11-02

#pragma once

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <ctype.h>

using namespace cv;
using namespace std;

namespace lkflow {

    // parameters structure define
    struct Params {
        // image resolution
        int image_width;
        int image_height;
        
        // subregion number
        int x_subregions;
        int y_subregions;
        
        // image boundary
        int x_boundary;
        int y_boundary;
        
        // maximum feature number in each subregion
        int max_subregion_features;
        
        // the total subregions
        int total_subregions;
        
        // the max feature number
        int max_features;
        
        // the subregion size
        double subregion_height;
        double subregion_width;
        
        // default parameters
        Params() {
            // image resolution
            image_width = 640;
            image_height = 480;
            
            // subregion number
            x_subregions = 6;
            y_subregions = 4;
            
            // image boundary
            x_boundary = 80;
            y_boundary = 80;
            
            // maximum feature number in each subregion
            max_subregion_features = 5;
            
            // caculate the other parameters accordingly
            total_subregions = x_subregions * y_subregions;
            max_features = max_subregion_features *
                total_subregions;
            subregion_height = ( image_height - 2 *
                    y_boundary ) / y_subregions;
            subregion_width = ( image_width - 2 *
                    x_boundary ) / x_subregions;
        }
    };
    
    class LKFlow {
        public:
        
            LKFlow();
            ~LKFlow();
            
            // set parameters
            void setParams ( const Params& params ) {
                params_ = params;
                params_.total_subregions = params_.x_subregions *
                    params_.y_subregions;
                params_.subregion_height = ( params_.image_height
                        - 2 * params_.y_boundary ) / params_.y_subregions;
                params_.subregion_width = ( params_.image_width -
                        2 * params_.x_boundary ) / params_.x_subregions;
                params_.max_features =
                    params_.max_subregion_features *
                    params_.total_subregions;
                    
            }
            
            // get parameters
            Params getParams() const { return params_; }
            
            // feature tracking function
            void featureTracking ( Mat &img_prev,
                Mat &img_cur, vector<Point2f> &points_prev,
                vector<Point2f> &points_cur );
            Mat B2Gmat ( float* att );
            Mat G2Bmat ( float* att );
            void fcnDispDoubleMat ( Mat& A );
            
        private:
            // parameters
            Params params_;
            // init flag
            bool need_init_;
            // feature id, not use now
            uint64 feature_id_;
            // store current feature number in every subregion
            vector<int>
            subregion_feature_num_;
    };
}
