//***************************************************************************************
//
//! \file klt_tracking_cls.cpp
//! This is a visual odometry (VO) based on optical flow and homography decomposition.
//!
//! \author    Yingcai BI, Xu YAN, and Feng LIN
//! \version   V1.0
//! \date      2017-11-02
//! \copyright GNU Public License V3.0
//
//***************************************************************************************

// This file is for optical flow based homography decomposition.
// Change the image path and sequence index inside the code before you use.
// Bi Yingcai, 2017-11-02

#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "klt_tracking_cls.h"

using namespace cv;
using namespace std;

namespace  ns_klt_tracking {

    double computeFeatureFlow ( vector<Point2f>& prev,
        vector<Point2f>& curr )
    {
        double total_flow = 0.0;
        
        for ( size_t i = 0; i < prev.size(); ++i ) {
            double x_diff = curr.at ( i ).x - prev.at ( i ).x;
            double y_diff = curr.at ( i ).y - prev.at ( i ).y;
            total_flow += sqrt ( x_diff * x_diff + y_diff *
                    y_diff );
        }
        
        return total_flow / prev.size();
    }
    
    // return position in NED
    //
    void  KLT_TRACKING::getPositionNED (
        double& _pos_x,
        double& _pos_y,
        double& _pos_z )
    {
        _pos_x = pos_x;
        _pos_y = pos_y;
        _pos_z = pos_z;
    }
    
    
    // initialization
    //
    KLT_TRACKING::KLT_TRACKING()
    {
        frame_num = 0;
        
        // type of the camera lens
        //
        // 0: simulated camera;
        // 1: Optris PI640 30 degree;
        // 2: Optris PI640 45 degree;
        //
        typeLens = 0;
        
        // initialize camera intrinsic matrix and distortion
        camera_intrinsic = Mat ( 3, 3,
                DataType<double>::type );
                
        camera_distort = Mat ( 4, 1,
                DataType<double>::type );
                
        if ( typeLens == 0 ) {
            camera_intrinsic.at<double> ( 0, 0 ) = 603.8792;
            camera_intrinsic.at<double> ( 0, 1 ) = 0;
            camera_intrinsic.at<double> ( 0, 2 ) = 191.5;
            camera_intrinsic.at<double> ( 1, 0 ) = 0;
            camera_intrinsic.at<double> ( 1, 1 ) = 603.8792;
            camera_intrinsic.at<double> ( 1, 2 ) = 144.5;
            camera_intrinsic.at<double> ( 2, 0 ) = 0;
            camera_intrinsic.at<double> ( 2, 1 ) = 0;
            camera_intrinsic.at<double> ( 2, 2 ) = 1;
            
            camera_distort.at<double> ( 0, 0 ) = 0.0;
            camera_distort.at<double> ( 1, 0 ) = 0.0;
            camera_distort.at<double> ( 2, 0 ) = 0.0;
            camera_distort.at<double> ( 3, 0 ) = 0.0;
        }
        else if ( typeLens == 1 ) {
            camera_intrinsic.at<double> ( 0, 0 ) = 600.94;
            camera_intrinsic.at<double> ( 0, 1 ) = 0;
            camera_intrinsic.at<double> ( 0, 2 ) = 195.8;
            camera_intrinsic.at<double> ( 1, 0 ) = 0;
            camera_intrinsic.at<double> ( 1, 1 ) = 601.43;
            camera_intrinsic.at<double> ( 1, 2 ) = 158.97;
            camera_intrinsic.at<double> ( 2, 0 ) = 0;
            camera_intrinsic.at<double> ( 2, 1 ) = 0;
            camera_intrinsic.at<double> ( 2, 2 ) = 1;
            
            camera_distort.at<double> ( 0, 0 ) = -0.65774;
            camera_distort.at<double> ( 1, 0 ) = 0.26176;
            camera_distort.at<double> ( 2, 0 ) = -0.01305;
            camera_distort.at<double> ( 3, 0 ) = 0.00254;
        }
        
        // override lkflow parameters
        params.image_width  = 382;
        params.image_height = 288;
        params.x_boundary   = 11;
        params.y_boundary   = 24;
        params.max_subregion_features = 10;
        params.x_subregions = 6;
        params.y_subregions = 4;
        lkFlow.setParams ( params );
        
        pp = Point2d (
                camera_intrinsic.at<double> ( 0, 2 ),
                camera_intrinsic.at<double> ( 1, 2 ) );
                
        fx = camera_intrinsic.at<double> ( 0, 0 );
        fy = camera_intrinsic.at<double> ( 1, 1 );
        
        // flag for the first frame initialization
        is_frame_initialized_ = false;
        
        // initialize the position
        pos_x = 0.0;
        pos_y = 0.0;
        pos_z = 0.0;
        
        // initialize the attitudes and heights
        imu_phi1    = 0.0;
        imu_tht1    = 0.0;
        imu_psi1    = 0.0;
        imu_att1[0] = 0.0;
        imu_att1[1] = 0.0;
        imu_att1[2] = imu_psi1;
        imu_phi2    = 0.0;
        imu_tht2    = 0.0;
        imu_psi2    = 0.0;
        imu_att2[0] = 0.0;
        imu_att2[1] = 0.0;
        imu_att2[2] = imu_psi2;
        
        // threshold
        ref_frame_motion_threshold_ = 5.0;
        
        // motion estimation method
        // 0: homography (small motion);
        // 1: (large motion) essential
        //
        method = 0;
        
    }
    
    
    // track in a new frame
    //
    void KLT_TRACKING::addFrame ( const cv::Mat&
        _image, float imu_phi, float imu_tht,
        float imu_psi, float laser_h )
    {
        // check the image
        if ( !_image.data ) {
            std::cout << "Image for klt tracking is empty" <<
                std::endl;
            return;
        }
        
        // copy image
        _image.copyTo ( image_cur );
        
        // convert the gray image to RGD for display
        cvtColor ( image_cur, image_show, CV_GRAY2RGB );
        
        // get current attitude from imu
        imu_phi2 = imu_phi;
        imu_tht2 = imu_tht;
        imu_psi2 = imu_psi;
        imu_att2[2] = imu_psi2;
        
        if ( !is_frame_initialized_ ) {
            //copy image
            image_cur.copyTo ( image_prev );
            
            // initialize the attitudes
            imu_phi2    = imu_phi;
            imu_tht2    = imu_tht;
            imu_psi2    = imu_psi;
            imu_att2[2] = imu_psi2;
            laser_h2    = laser_h;
            
            imu_phi1    = imu_phi2;
            imu_tht1    = imu_tht2;
            imu_psi1    = imu_psi2;
            imu_att1[2] = imu_psi1;
            laser_h1    = laser_h2;
            
            // initialize the lkflow
            lkFlow.featureTracking ( image_prev, image_cur,
                points_prev, points_cur );
                
            is_frame_initialized_ = true;
        }
        else {
            imu_phi2 = imu_phi;
            imu_tht2 = imu_tht;
            imu_psi2 = imu_psi;
            imu_att2[2] = imu_psi2;
            laser_h2 = laser_h;
            
            // feature tracking
            lkFlow.featureTracking ( image_prev, image_cur,
                points_prev, points_cur );
                
                
            /*  if (points_prev.size() < 8) {
                a1 = a2; b1 = b2; c1 = c2; h1 = h2;
            
            
                std::cout << "frame " << frame_num << ": " <<
                    "Failed" << endl;
                return;
                }*/
            
            // extract corresponding feature points
            un_points_cur.clear();
            un_points_prev.clear();
            
            undistortPoints ( points_prev, un_points_prev,
                camera_intrinsic, camera_distort, Mat(),
                camera_intrinsic );
                
            undistortPoints ( points_cur, un_points_cur,
                camera_intrinsic, camera_distort, Mat(),
                camera_intrinsic );
                
            // pre-processing
            /*
                double flow;
                flow = computeFeatureFlow(un_points_prev, un_points_cur);
                vector<Point2f> good_points_prev, good_points_curr;
                for (int iter = 0; iter < un_points_prev.size(); iter++) {
                double dx = un_points_prev.at(iter).x - un_points_cur.at(iter).x;
                double dy = un_points_prev.at(iter).y - un_points_cur.at(iter).y;
                double dis = sqrt(dx*dx + dy*dy);
                // if (dis < 5 * flow) {
                good_points_prev.push_back(Point2f(un_points_prev.at(iter).x, un_points_prev.at(iter).y));
                good_points_curr.push_back(Point2f(un_points_cur.at(iter).x, un_points_cur.at(iter).y));
                // }
                }
                flow = computeFeatureFlow(good_points_prev, good_points_curr);
            
                if (flow > ref_frame_motion_threshold_)
                method = 1;
                else
                method = 0;
            */
            
            /*
                Point2f pcam;
                vector<Point2f> un_pcam_prev, un_pcam_cur;
                for (size_t i = 0; i < un_points_prev.size(); i++)
                {
                pcam.x = -(un_points_prev[i].y - pp.y) / fy;
                pcam.y = (un_points_prev[i].x - pp.x) / fx;
                un_pcam_prev.push_back(pcam);
                pcam.x = -(un_points_cur[i].y - pp.y) / fy;
                pcam.y = (un_points_cur[i].x - pp.x) / fx;
                un_pcam_cur.push_back(pcam);
                }
            */
            
            if ( method == 1 ) {
                Mat R = Mat ( 3, 3, CV_32FC1 );
                Mat t = Mat ( 3, 1, CV_32FC1 );
                
                // compute essential matrix
                Mat E, mask;
                E = findEssentialMat ( un_points_prev,
                        un_points_cur, fx, pp, RANSAC, 0.999, 3, mask );
                        
                // compute R and t from essential matrix
                recoverPose ( E, un_points_cur, un_points_prev, R,
                    t, fx, pp, mask );
                    
                // plot feature correspondence and outliers on image
                if ( plot_feature_matching_flag ) {
                
                    for ( size_t j = 0; j < un_points_cur.size();
                        j++ ) {
                        
                        if ( mask.at<uchar> ( j, 0 ) == 1 ) {
                            circle ( image_show, un_points_cur[j], 2,
                                Scalar ( 255, 0, 0 ), -1, 8 );
                            line ( image_show, un_points_prev[j],
                                un_points_prev[j] + 2 * ( un_points_cur[j] -
                                    un_points_prev[j] ), Scalar ( 0, 255, 0 ) );
                        }
                        else {
                            circle ( image_show, un_points_cur[j], 2,
                                Scalar ( 0, 0, 255 ), -1, 8 );
                            line ( image_show, un_points_prev[j],
                                un_points_prev[j] + 2 * ( un_points_cur[j] -
                                    un_points_prev[j] ), Scalar ( 0, 0, 255 ) );
                        }
                    }
                }
                
                // recover scale using laser estimated height of the central point
                double min_dist = 2 * pp.x;
                double c_norm = 0;
                double scale = 0;
                
                for ( int i = 0; i < un_points_prev.size();
                    i++ ) {
                    
                    double dist = sqrt ( ( un_points_prev.at (
                                    i ).x - pp.x ) * ( un_points_prev.at (
                                    i ).x - pp.x )
                            + ( un_points_prev.at ( i ).y - pp.y ) *
                            ( un_points_prev.at ( i ).y - pp.y ) );
                            
                    if ( dist < min_dist ) {
                        min_dist = dist;
                        c_norm = sqrt ( ( un_points_prev.at (
                                        i ).x - un_points_cur.at ( i ).x ) *
                                ( un_points_prev.at ( i ).x - un_points_cur.at (
                                        i ).x )
                                + ( un_points_prev.at ( i ).y - un_points_cur.at (
                                        i ).y ) * ( un_points_prev.at ( i ).y -
                                    un_points_cur.at ( i ).y ) );
                    }
                }
                
                double t_norm = sqrt ( t.at<double> ( 0,
                            0 ) * t.at<double> ( 0, 0 ) + t.at<double> ( 1,
                            0 ) * t.at<double> ( 1, 0 ) );
                            
                if ( c_norm < 10 && t_norm > 0.01 )
                { scale = ( laser_h1 ) * c_norm / t_norm / fx; }
                
                pos_x += t.at<double> ( 0, 0 ) * scale;
                pos_y += t.at<double> ( 1, 0 ) * scale;
                pos_z += t.at<double> ( 2, 0 ) * scale;
                std::cout << "frame " << frame_num << ": " <<
                    "Essential" << endl;
            }
            else {
                // find homography and outliers
                Mat homo_mask;
                
                // double tolerance = 0.01;
                // Mat H = findHomography(un_pcam_prev, un_pcam_cur, CV_RANSAC, tolerance, homo_mask);
                double tolerance = 0.4;
                Mat H = findHomography ( un_points_prev,
                        un_points_cur, CV_RANSAC, tolerance, homo_mask );
                        
                        
                // plot feature correspondence and outliers on image
                if ( plot_feature_matching_flag ) {
                    for ( size_t j = 0; j < un_points_cur.size();
                        j++ ) {
                        if ( homo_mask.at<uchar> ( j, 0 ) == 1 ) {
                            circle ( image_show, un_points_cur[j], 2,
                                Scalar ( 255, 0, 0 ), -1, 8 );
                            line ( image_show, un_points_prev[j],
                                un_points_prev[j] + 2 * ( un_points_cur[j] -
                                    un_points_prev[j] ), Scalar ( 0, 255, 0 ) );
                        }
                        else {
                            circle ( image_show, un_points_cur[j], 2,
                                Scalar ( 0, 0, 255 ), -1, 8 );
                            line ( image_show, un_points_prev[j],
                                un_points_prev[j] + 2 * ( un_points_cur[j] -
                                    un_points_prev[j] ), Scalar ( 0, 0, 255 ) );
                        }
                    }
                }
                
                double phi = 0.0;
                double tht = 0.0;
                double psi = imu_att1[2];
                
                // pixel-wise
                Mat R = Mat::eye ( 3, 3, DataType<double>::type );
                R = lkFlow.G2Bmat ( imu_att2 ) * lkFlow.B2Gmat (
                        imu_att1 );
                H = camera_intrinsic.inv ( DECOMP_SVD ) * H *
                    camera_intrinsic;
                SVD svd ( H );
                H /= svd.w.at<double> ( 1 );
                H -= R;
                double N_data[] = { -sin ( tht ), sin ( phi )* cos ( tht ), cos ( phi )* cos ( tht ) };
                Mat N = Mat ( 3, 1, DataType<double>::type,
                        N_data ).clone();
                Mat T = H * - laser_h2 * N;
                
                // metric-wise
                /*
                    Mat R = Mat::eye(3, 3, DataType<double>::type);
                    R = lkFlow.G2Bmat(att2) * lkFlow.B2Gmat(att1);
                    SVD svd(H);
                    H /= svd.w.at<double>(1);
                    H -= R;
                    double N_data[] = { -sin(tht), sin(phi)*cos(tht), cos(phi)*cos(tht) };
                    Mat N = Mat(3, 1, DataType<double>::type, N_data).clone();
                    Mat T = H * h2 * N;
                    T = -lkFlow.B2Gmat(att2) * T;
                */
                
                pos_x += T.at<double> ( 0 );
                pos_y += T.at<double> ( 1 );
                pos_z += T.at<double> ( 2 );
                std::cout << "frame " << frame_num << ": " <<
                    "Homography" << endl;
            }
            
            /*
                // find homography and outliers
                cv::Mat homo_mask;
                double tolerance = 0.5;
                Mat H = findHomography(un_points_prev, un_points_cur, CV_RANSAC, tolerance, homo_mask);
            
                // plot feature correspondence and outliers on image
                for (size_t j = 0; j < points_cur.size(); j++)
                {
                if(homo_mask.at<uchar>(0,j)==1)
                {
                circle(image_show, points_cur[j], 2, Scalar(255, 0, 0), -1, 8);
                line(image_show, points_prev[j], points_prev[j] + 2 * (points_cur[j]-points_prev[j]), Scalar(0, 255, 0));
                }
                else
                {
                circle(image_show, points_cur[j], 2, Scalar(0, 0, 255), -1, 8);
                line(image_show, points_prev[j], points_prev[j] + 2 * (points_cur[j]-points_prev[j]), Scalar(0, 0, 255));
                }
                }
            
                // decomposite homography to get translation by assuming
                SVD svd(H);
                // H /= svd.w.at<double>(1);
                H -= Mat::eye(3,3,CV_64F);
                Mat N = (Mat_<double>(3,1) << 0, 0, 1);
                double h = 40;
                Mat T = H * h* N;
                x += T.at<double>(0)/1000;
                y += T.at<double>(1)/1000;
                z += T.at<double>(2)/1000;
            */
            /*
                double ry = asin(R.at<double>(0, 2));
                double rx = asin(-R.at<double>(1, 2) / cos(ry));
                double rz = asin(-R.at<double>(0, 1) / cos(ry));
            
                // return parameter vector
                vector<double> tr_delta;
                tr_delta.resize(6);
                tr_delta[0] = rx;
                tr_delta[1] = ry;
                tr_delta[2] = rz;
                tr_delta[3] = t.at<double>(0, 0) * scale;
                tr_delta[4] = t.at<double>(1, 0) * scale;
                tr_delta[5] = t.at<double>(2, 0) * scale;
            
                Matrix rt;
                rt = transformationVectorToMatrix(tr_delta);
            
                pose = pose = pose * Matrix::inv(rt);
            
                // print position output
                cout << "frame" << frame_num << ":" << fixed << setprecision(4) << endl;
                cout << pose << endl;
            */
            
            std::cout << pos_x << " " << pos_y << " " << pos_z
                << fixed <<
                setprecision ( 4 ) << endl;
                
            imu_phi1 = imu_phi2;
            imu_tht1 = imu_tht2;
            imu_psi1 = imu_psi2;
            laser_h1 = laser_h2;
            
            // show feature correspondence and outliers on imagewith some delay
            if ( plot_feature_matching_flag ) {
                namedWindow ( "Display", WINDOW_AUTOSIZE );
                imshow ( "Display", image_show );
                cv::waitKey ( 10 );
            }
        }
        
        frame_num++;
    }
    
    KLT_TRACKING::~KLT_TRACKING()
    {
    }
}