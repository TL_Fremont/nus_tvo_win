//***************************************************************************************
//
//! \file main.cpp
//! This is a main entry for a thermal visual odometry.
//!
//! \author    Yingcai BI, Xu YAN, and Feng LIN
//! \version   V1.0
//! \date      2017-11-02
//! \copyright GNU Public License V3.0
//
//***************************************************************************************

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "iostream"
#include "stdio.h"
#include "iomanip"
#include "stdint.h"
#include "klt_tracking_cls.h"

using namespace cv;
using namespace std;
using namespace ns_klt_tracking;


//! Application Entry
int main ( int argc, char** argv )
{
    // read data from text file
    ifstream data;
    //! Change the image path and sequence index inside the code before you use.
    data.open ( "K:/P_FRUITDOVE/Research-Perching/tvo_win/data.txt",
        ios_base::in );
        
    // record output data
    //! Change the image path and sequence index inside the code before you use.
    ofstream poseFile ( "K:/P_FRUITDOVE/Research-Perching/tvo_win/pose.txt" );
    
    // image variables
    char filename[100];
    
    // initialize the klt_tracking
    KLT_TRACKING ktl_tracking;
    
    // imu and laser data
    float imu_phi = 0.0, imu_tht = 0.0, imu_psi = 0.0,
          laser_h = 0.0;
          
    // estimated position
    double pos_x = 0.0, pos_y = 0, pos_z = 0;
    
    // read orienation and gps height from text
    // if need to jump a step
    string lines;
    for ( int step = 0; step < 1; step ++ ) {
        getline ( data, lines );
    }
    
    // main loop to process images
    for ( int frame_num = 1; frame_num < 2750;
        frame_num = frame_num + 1 ) {
        
        getline ( data, lines );
        //make a stream for the line itself
        istringstream in ( lines );
        
        in >> imu_phi >> imu_tht >> imu_psi >>
            laser_h;       //now read the whitespace-separated floats
            
        // read image
        sprintf ( filename,
            "K:/P_FRUITDOVE/Thermal/data/thermal_sim_2017-11-07/image/thermal_image%d.png",
            frame_num );
            
        Mat _image = imread ( filename,
                CV_LOAD_IMAGE_GRAYSCALE );
                
        if ( !_image.data ) {
            std::cout << "Could not open image" << std::endl;
            return -1;
        }
        
        ktl_tracking.addFrame ( _image, imu_phi, imu_tht,
            imu_psi, laser_h );
            
        ktl_tracking.getPositionNED ( pos_x, pos_y,
            pos_z );
            
        // save the estimated position to a file
        poseFile << pos_x << "\t" << pos_y << "\t" <<
            pos_z << endl;
    }
    
    system ( "pause" );
    // cv::waitKey(0);
    
    return 0;
}
