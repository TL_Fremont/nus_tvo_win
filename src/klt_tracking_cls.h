//***************************************************************************************
//
//! \file klt_tracking_cls.h
//! This is the header file of a visual odometry (VO) based on optical flow and homography decomposition.
//!
//! \author    Yingcai BI, Xu YAN, and Feng LIN
//! \version   V1.0
//! \date      2017-11-02
//! \copyright GNU Public License V3.0
//
//***************************************************************************************

#include <opencv2/opencv.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

#include "iostream"
#include "stdio.h"
#include "iomanip"
#include "stdint.h"
#include "lkflow.h"

using namespace cv;
using namespace std;


//! plot feature correspondence and outliers on image, 0: not plot, 1: plot
#define plot_feature_matching_flag 0

namespace ns_klt_tracking {

    class KLT_TRACKING {
        public:
        
            KLT_TRACKING();
            ~KLT_TRACKING();
            
            //***************************************************************************************
            //
            //! \brief Add a new image and UAV states to the VO
            //!
            //! \param [in] _image is the new image
            //! \param [in] imu_phi is the roll angle from IMU
            //! \param [in] imu_tht is the pitch angle from IMU
            //! \param [in] imu_psi is the yaw angle from IMU
            //
            //***************************************************************************************
            void addFrame ( const cv::Mat& _image,
                float imu_phi, float imu_tht, float psi,
                float h );  // add a new frame for tracking
                
                
            //***************************************************************************************
            //
            //! \brief Get 3D position output from VO in north-east-down (NED) frame
            //!
            //! \param [out] _pos_x is the position coordinate in x-axis
            //! \param [out] _pos_y is the position coordinate in x-axis
            //! \param [out] _pos_z is the position coordinate in x-axis
            //
            //***************************************************************************************
            void getPositionNED ( double& _pos_x,
                double& _pos_y,
                double& _pos_z ); // return position in NED
                
        private:
        
            int frame_num;
            
            
            // flag
            bool is_frame_initialized_;
            
            // estimated position
            double pos_x;
            double pos_y;
            double pos_z;
            
            // Euler angles and height at frame #1
            float imu_phi1;
            float imu_tht1;
            float imu_psi1;
            float laser_h1;
            float imu_att1[3]; // attitude of the camera on the gimbal
            
            // Euler angles and height at frame #2
            float imu_phi2;
            float imu_tht2;
            float imu_psi2;
            float laser_h2;
            float imu_att2[3]; // attitude of the camera on the gimbal
            
            // threshold
            double ref_frame_motion_threshold_;
            
            // motion estimation method
            // 0: homography (small motion); 1: (large motion) essential
            //
            int method;
            
            // images
            Mat image_prev, image_cur, image_show;
            
            vector<Point2f> points_prev, points_cur;
            vector<Point2f> un_points_prev, un_points_cur;
            
            // camera intrinsic matrix and distortion
            Mat camera_intrinsic;
            Mat camera_distort;
            
            Point2d pp;
            double fx;
            double fy;
            
            // type of the camera lens
            //
            // 0: simulated camera;
            // 1: Optris PI640 30 degree;
            // 2: Optris PI640 45 degree;
            //
            int typeLens;
            
            // lkflow definition
            lkflow::Params params;
            lkflow::LKFlow lkFlow;
    };
}